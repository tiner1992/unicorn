import * as THREE from 'three';
import GLTFLoader from 'three-gltf-loader';

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
const loader = new GLTFLoader();
var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );
var geometry = new THREE.BoxGeometry( 1, 1, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
var cube = new THREE.Mesh( geometry, material );
var spotLight = new THREE.SpotLight( 0xffffff );
spotLight.position.set( 10, 10, 10 );
scene.add( spotLight );
var spotLightHelper = new THREE.SpotLightHelper( spotLight );
scene.add( spotLightHelper );
scene.add( cube );
camera.position.z = 5;
var elemInFocus = camera;
var model = undefined;
var animate = function () {
    requestAnimationFrame( animate );
    cube.rotation.x += 0.01;
    cube.rotation.y += 0.01;
    renderer.render( scene, camera );
};

document.addEventListener('keydown', function(e){
    if(e.which === 65){  // A -btn
        elemInFocus.position.x -= 0.2;
    }else if(e.which === 83){ //S -btn
        elemInFocus.position.y -= 0.2;
    }else if(e.which === 68){ //D -btn
        elemInFocus.position.x += 0.2;
    }else if(e.which === 87){ //W -btn
        elemInFocus.position.y += 0.2;
    }else if(e.which === 90){ //Z -btn
        elemInFocus.position.z-= 0.2;
    }else if(e.which === 88){ //X -btn
        elemInFocus.position.z+= 0.2;
    }else if(e.which === 81) { // Q- clicked
        if(elemInFocus === camera){
            elemInFocus = spotLight;
        }else{
            elemInFocus = camera;
        }
    }
    console.log(camera.matrixWorldInverse);
});

animate();
loader.load(
    "./Unicorn_01.gltf",
    ( gltf ) => {
        // called when the resource is loaded
        scene.add( gltf.scene );
        model = gltf.scene;
    },
    ( xhr ) => {
        // called while loading is progressing
        console.log( `${( xhr.loaded / xhr.total * 100 )}% loaded` );
    },
    ( error ) => {
        // called when loading has errors
        console.error( 'An error happened', error );
    },
);
/*import bin from './Unicorn_01.bin';
 
const loader = new GLTFLoader();
var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
var geometry = new THREE.BoxGeometry( 1, 1, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0x00ff00 } );
var renderer = new THREE.WebGLRenderer();
var cube = new THREE.Mesh( geometry, material );
scene.add( cube );
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );
loader.load(
    "./Unicorn_01.gltf",
    ( gltf ) => {
        // called when the resource is loaded
        console.log(gltf);
        scene.add( gltf.scene );
    },
    ( xhr ) => {
        // called while loading is progressing
        console.log( `${( xhr.loaded / xhr.total * 100 )}% loaded` );
    },
    ( error ) => {
        // called when loading has errors
        console.error( 'An error happened', error );
    },
);

function animate() {
	requestAnimationFrame( animate );
	renderer.render( scene, camera );
}
animate();*/