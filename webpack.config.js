const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
      },
  entry: './src/index.js',
  output: {
    filename: 'index.js',
    path: path.resolve(__dirname, 'dist')
  },
  plugins: [
         new HtmlWebpackPlugin({
           title: 'Some title',
           template: './index.html'
         })
       ],
       module: {
        rules: [
       {
           test: /\.(gltf|bin)$/,
           use: [
             'file-loader'
           ]
         }
        ]
      }
};